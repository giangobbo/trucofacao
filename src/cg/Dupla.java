/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cg;

/**
 *
 * @author Gianfranco
 */
public class Dupla {
    
    private Jogador jogadores[]= new Jogador[2];
    private int pontos=0;

    public Dupla(Jogador jogador1,Jogador jogador2) {
        this.jogadores[0]=jogador1;
        this.jogadores[1]=jogador2;
    }
    
    public boolean pertence(Jogador jogador){
        for(int i=0;i<jogadores.length;i++)
            if(jogadores[i].equals(jogador)) return true;
        return false;
    }
    
    public void setPontos(int pontos) {
        this.pontos += pontos;
    }

    public int getPontos() {
        return pontos;
    }
    

    @Override
    public String toString() {
        return jogadores[0].getNome()+" e "+jogadores[1].getNome();
    }
    
}

