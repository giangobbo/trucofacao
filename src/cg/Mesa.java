/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cg;

/**
 *
 * @author Henike
 */
public class Mesa{
    
    private Carta cartas[]=new Carta[4];
    private int posicao=0, num=0;
    private Jogador vencedor=null,jogadores[];
    
    
    public Jogador getVencedor() {
        return vencedor;
    }
    
    //a carta da posicao 1 é do jogador 1, da posicao 0 é do jogador 0
    public Mesa(Jogador[] jogadores, int num) {      
        this.jogadores=jogadores;
        for(int i=0;i<cartas.length;i++)
            cartas[i] = null;
        this.num=num;
    }
    
    public boolean isCompleta(){
        for (Carta carta : cartas) 
            if (carta == null) return false;
        return true;  
    }

    public Carta[] getCartas() {
        return cartas;
    }

    public void setCartas(int pos, Carta carta, Jogador jog) {
        this.cartas[pos]=carta;    
    }

    public Carta maior(){
        if(cartas[0]==null) cartas[0]=new Carta(0, Naipe.OURO);
        if(cartas[1]==null) cartas[1]=new Carta(0, Naipe.OURO);
        if(cartas[2]==null) cartas[2]=new Carta(0, Naipe.OURO);
        if(cartas[3]==null) cartas[3]=new Carta(0, Naipe.OURO);

        if(cartas[0].getValor()>cartas[1].getValor() && cartas[0].getValor()>cartas[2].getValor() && cartas[0].getValor()>cartas[3].getValor())
            return cartas[0];
        else if(cartas[1].getValor()>cartas[0].getValor() && cartas[1].getValor()>cartas[2].getValor() && cartas[1].getValor()>cartas[3].getValor())
            return cartas[1];
        else if(cartas[2].getValor()>cartas[0].getValor() && cartas[2].getValor()>cartas[1].getValor() && cartas[2].getValor()>cartas[3].getValor())
            return cartas[2];
        return cartas[3];            
    }
    
    
    public int indiceMaior(){
        Carta carta = this.maior();
        for(int i=0;i<cartas.length;i++)
            if(carta.equals(cartas[i]))
                return i;
        return -1;    
    }
    
    //define quem venceu a mesa (null=empate)
    public void setVencedor(){        
        Carta carta = this.maior();
        for (Jogador jog : jogadores) {
            if (jog.pertence(carta)) 
                vencedor = jog;
        }
        System.out.println("Vencedor da mesa "+num+": "+vencedor+" carta vencedora: "+carta);
        //vencedor=null;
    }

    @Override
    public String toString() {
        return "Mesa: " +num +" cartas: " +cartas[0]+" - "+cartas[1]+" - "+cartas[2]+" - "+cartas[3] ;
    } 
}

