/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cg;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Felipe
 */
public class Carta {
    
    private String nome;
    private int valor;
    private int posicao[];
    private BufferedImage imagem;
    private String fileName;
    private Naipe naipe;
    private boolean saiu=false, jaJoagada=false;

    public void jaJoagada() {
        jaJoagada = true;
    }

    public boolean isJaJoagada() {
        return jaJoagada;
    }
    
    public Carta(int valor, Naipe naipe) {
        this.valor = valor;
        this.naipe=naipe;
        this.fileName=System.getProperty("user.dir")+"\\imagens\\"+Integer.toString(valor)+naipe.getCodNaipe()+".png";      
                
        switch (this.valor){
            case 0: nome="quatro";break;
            case 1: nome="cinco";break;
            case 2: nome="seis";break;
            case 3: nome="sete";break;
            case 4: nome="dama";break;
            case 5: nome="valete";break;
            case 6: nome="rei";break;
            case 7: nome="as";break;
            case 8: nome="dois";break;
            case 9: nome="tres";break;
            default:;
        }
        
        posicao = new int[2];
        
        try {
            imagem = ImageIO.read(new File(fileName));
        } catch (IOException e) {
            System.out.println("Erro no carregamento da imagem da carta :"+this);
        }
    }

    public int[] getPosicao() {
        return posicao;
    }

    public void setPosicao(int[] posicao) {
        this.posicao = posicao;
    }
 
    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor += valor;
    }

    public BufferedImage getImagem() {
        return imagem;
    }

    public void setImagem(BufferedImage imagem) {
        this.imagem = imagem;
    }

    public boolean isSaiu() {
        return saiu;
    }

    public void setSaiu(boolean saiu) {
        this.saiu = saiu;
    }

    public boolean ehManilha() {
        return valor>=1000;
    }
   
    @Override
    public String toString() {
        return nome+" de "+naipe.getDescricao()+" valor: "+valor;
    } 
}
