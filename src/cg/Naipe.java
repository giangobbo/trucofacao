/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cg;

/**
 *
 * @author Felipe
 */
public enum Naipe {
    
    OURO,
    ESPADA,
    COPAS,
    PAUS; 
    
    public String getDescricao(){
        switch(this){
            case OURO: return "ouro";
            case ESPADA: return "espada";
            case COPAS: return "copas";
            case PAUS: return "paus";
            default: return "";
        }
    }
    
    public int getCodNaipe(){
        switch(this){
            case OURO: return 0;
            case ESPADA: return 2;
            case COPAS: return 3;
            case PAUS: return 4;
            default:return -1;
        }
    }
}
