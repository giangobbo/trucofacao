package cg;

import java.awt.*;
import java.awt.image.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;

public class BufferedImageDrawer extends Frame {

    public BufferedImage bi;

    public Graphics2D g2dbi;

    private Graphics2D g2d;

    public boolean cima, baixo, esquerda, direita;
    
    private int select=0;

    public int getSelect() {
        return select;
    } 
    
    public BufferedImageDrawer(BufferedImage buffIm, int width, int height) {

        cima = baixo = esquerda = direita = false;

        bi = buffIm;

        g2dbi = bi.createGraphics();

        addWindowListener(new MyFinishWindow());
        addKeyListener(new KeyInputHandler());

        this.setTitle("Truco Facãozero");
        this.setSize(width, height);
        this.setVisible(true);

    }

    @Override
    public void paint(Graphics g) {
        update(g);
    }

    @Override
    public void update(Graphics g) {
        int i;

        g2d = (Graphics2D) g;

        g2d.drawImage(bi, 0, 0, null);
    }

    private class KeyInputHandler extends KeyAdapter {

        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                esquerda = true;
                select--;
                if(select==-1) select=2;                
            }
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                direita = true;
                select++;
                if(select==3) select=0;
                
            }
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                cima = true;
            }
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                baixo = true;
            }
        }

        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                esquerda = false;
            }
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                direita = false;
            }
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                cima = false;
            }
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                baixo = false;
            }
        }

        public void keyTyped(KeyEvent e) {
            if (e.getKeyChar() == 27) {
                System.exit(0);
                
                        
            }
        }
    }

}
