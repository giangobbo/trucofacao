/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cg;

/**
 *
 * @author Henike
 */
public class Decisao {
    
    private Carta carta;
    
    public void decide(int vez, Jogador jog, Mesa mesa, Carta minhaCarta){
         
        switch(vez){
            //o jogador é o primeiro a jogar
            case 0:             
                if(jog.getNum()==2 && !minhaCarta.isJaJoagada())
                    carta=minhaCarta;
                else carta=jog.getCarta(jog.indiceMaior());
                mesa.setCartas(jog.getNum(), carta, jog);
                break;
            case 1:            
                if(jog.getNum()==2 && !minhaCarta.isJaJoagada())
                    carta=minhaCarta;
                else{
                    carta=jog.getCarta(jog.indiceMaior());
                    // se ele não tem uma carta maior q a do primeiro jogador ele joga a menor
                    if(carta.getValor()<= mesa.maior().getValor())
                        carta=jog.getCarta(jog.indiceMenor());
                }
                mesa.setCartas(jog.getNum(), carta, jog);
                break;
            default:
                if(jog.getNum()==2 && !minhaCarta.isJaJoagada())
                    carta=minhaCarta;
                else{
                    if(!this.verificaCarta(jog, mesa) && carta.getValor()>= mesa.maior().getValor())
                        carta=jog.getCarta(jog.indiceMaior());
                    else carta=jog.getCarta(jog.indiceMenor());
                }
                mesa.setCartas(jog.getNum(), carta, jog);
                break;
        }
        carta.jaJoagada();
        if(mesa.isCompleta()){
            mesa.setVencedor();
            System.out.println(mesa);
        }
    }
    
    // verifica se a carta da mesa foi jogada pelo parceiro
    private boolean verificaCarta(Jogador jog, Mesa mesa){
        // o numero da dupla difere sempre por 2
        return (jog.getNum()-mesa.indiceMaior())%2==0;
    }
}