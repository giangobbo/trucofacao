/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cg;

/**
 *
 * @author Felipe
 */
public class Jogador {
    
    private String nome;
    private int num;
    private Carta cartas[]= new Carta[3];

    public Jogador(String nome,int num) {
        this.nome=nome;
        this.num = num;
        
    }
    
    public void setCartas(Carta carta1, Carta carta2, Carta carta3){
        this.cartas[0] = carta1;
        this.cartas[1] = carta2;
        this.cartas[2] = carta3;
    }
    
    public String getNome() {
        return nome;
    }

    public int getNum() {
        return num;
    }

    public Carta getCarta(int index) {
        if(index==0) return cartas[0];
        else if(index==1)return cartas[1];
        return cartas[2];
    }
    
    // 2 eh menor mais ja foi jogada
    public int indiceMaior(){        
        if(cartas[0].isJaJoagada() && cartas[1].isJaJoagada()) return 2;
        else if(cartas[0].isJaJoagada() && cartas[2].isJaJoagada()) return 1;
        else if(cartas[1].isJaJoagada() && cartas[2].isJaJoagada()) return 0; 
        else if(cartas[0].isJaJoagada()){
            if(cartas[1].getValor()>=cartas[2].getValor())
                return 1; 
            return 2;   
        }
        else if(cartas[1].isJaJoagada()){
            if(cartas[0].getValor()>=cartas[2].getValor())
                return 0; 
            return 2;   
        }    
        else if(cartas[2].isJaJoagada()){
            if(cartas[0].getValor()>=cartas[1].getValor())
                return 0; 
            return 1;   
        }     
        else{
            if(cartas[0].getValor()>=cartas[1].getValor() && cartas[0].getValor()>=cartas[2].getValor())
                return 0;
            else if(cartas[1].getValor()>=cartas[0].getValor() && cartas[1].getValor()>=cartas[2].getValor())
                return 1;      
        }
        return 2;
    }
    
    public int indiceMenor(){
        if(cartas[0].isJaJoagada() && cartas[1].isJaJoagada()) return 2;
        else if(cartas[0].isJaJoagada() && cartas[2].isJaJoagada()) return 1;
        else if(cartas[1].isJaJoagada() && cartas[2].isJaJoagada()) return 0; 
        else if(cartas[0].isJaJoagada()){
            if(cartas[1].getValor()<=cartas[2].getValor())
                return 1; 
            return 2;   
        }
        else if(cartas[1].isJaJoagada()){
            if(cartas[0].getValor()<=cartas[2].getValor())
                return 0; 
            return 2;   
        }    
        else if(cartas[2].isJaJoagada()){
            if(cartas[0].getValor()<=cartas[1].getValor())
                return 0; 
            return 1;   
        }     
        else{
            if(cartas[0].getValor()<=cartas[1].getValor() && cartas[0].getValor()<=cartas[2].getValor())
                return 0;
            else if(cartas[1].getValor()<=cartas[0].getValor() && cartas[1].getValor()<=cartas[2].getValor())
                return 1;      
        }
        return 2;
    }
    
    public void tiraCarta(int index){
        this.cartas[index].jaJoagada();
    }
    
    public boolean pertence(Carta carta){
        
        if(carta!=null)
            for(Carta carta1 : cartas) {
                if(carta.equals(carta1)) 
                    return true;
            }
        return false;                
    }
    
    
    @Override
    public String toString() {
        return nome + ", cartas = " + cartas[0] +" - "+ cartas[1]+" - " + cartas[2];
    }    
}
