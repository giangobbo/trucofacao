package cg;

import java.awt.*;
import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Felipe
 */
public class Truco extends TimerTask {

    BufferedImageDrawer buffid;
    private  Rectangle2D rect;
    private  Ellipse2D round, cacapa, bola;
    private  int largura, altura,x=0,select=1,posicao[],nrodada=0,nmesa=0;
    
    private  String nomes[]=new String[]{"Sandy Devassa","Chico Bioca","Cumpadi Wóshto","Chimbinha","Mr Catra","Goku","Jaspion","Kréber","Jackso","Nego Zulu"};
    private  ArrayList<String> names = new ArrayList<>();
    Carta cartas[] = new Carta[3];

    private  Carta carta[][] = new Carta[10][4],vira,cartadavez=null;;    
    private  Mesa ultima;
    private  int indice_manilha,index = -1,frames=0,caminhosx[]={530,30,1200,250},caminhosy[]={100,250,250,530},segAnimacao=1,cont=0,j1=0, k1=0, l1, i1;
    private  double passos[][]=new double[4][2],fps=0;
    private  Jogador jogadores[] = new Jogador[4];
    private  Dupla dupla1, dupla2;
    private  Rodada rodadas[] = new Rodada[99];
    private  Decisao decisao = new Decisao();
    private BufferedImage versoCarta,espaço;
    private long time;     
    private boolean mostra = true, esperaeu=false, entrajog=true,escolheu=false;     
 

    
    public Jogador[] getJogadores() {
        return jogadores;
    }

    public Truco(BufferedImageDrawer bid, int width, int height, int delay) throws IOException {

        buffid = bid;
        round = new Ellipse2D.Double(600, 300, 30, 30);
        largura = width;
        altura = height;
        rect = new Rectangle2D.Double(0, 0, 50, 50);
        posicao = new int[2];
        
        names.addAll(Arrays.asList(nomes));
        Collections.shuffle(names);
        
        for(int j=0;j<jogadores.length;j++)
            jogadores[j] = new Jogador(names.get(j),j);
        
        this.insereCartas();
        /*for(int i=0;i<10;i++)
         for(int j=0;j<4;j++)
         System.out.println(carta[i][j].toString());*/
        this.vira();
        this.daCarta();
        this.time=System.currentTimeMillis();
        
        for(int i=0;i<rodadas.length;i++)
            rodadas[i]= new Rodada(jogadores,i+1);
        
        dupla1 = new Dupla(jogadores[0], jogadores[2]);
        dupla2 = new Dupla(jogadores[1], jogadores[3]);
        System.out.println("Dupla 1:"+dupla1);
        System.out.println("Dupla 2:"+dupla2);
        this.carregaVerso(); 
        fps=(double)1000/delay;
        for(int j=0; j<4;j++){         
            passos[j][0]=(double)caminhosx[j]/(fps*segAnimacao);
            passos[j][1]=(double)caminhosy[j]/(fps*segAnimacao);
        }
    }
    

    public void insereCartas() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 4; j++) {
                if (j == 0) {
                    carta[i][j] = new Carta(i, Naipe.OURO);
                } else if (j == 1) {
                    carta[i][j] = new Carta(i, Naipe.ESPADA);
                } else if (j == 2) {
                    carta[i][j] = new Carta(i, Naipe.COPAS);
                } else {
                    carta[i][j] = new Carta(i, Naipe.PAUS);
                }
            }
        }
    }

    public void vira() {
        Random gerador=new Random();        
        int a1, a2;
        a1=gerador.nextInt(10);
        a2=gerador.nextInt(4);
        vira=carta[a1][a2];
        carta[a1][a2].setSaiu(true);
        System.out.println("vira: " + carta[a1][a2].toString());
        indice_manilha = (a1 + 1) % 10;
        for (int i=0;i<4;i++) {
            carta[indice_manilha][i].setValor(1000 + i);
        }
        /*mostra todas as cartas com valor
         for(int i=0;i<10;i++)
         for(int j=0;j<4;j++)
         System.out.println(carta[i][j].toString());*/
    }


    public void daCarta(){
        int i=0;
        Random gerador=new Random();
        boolean saiu=false;
        int a1, a2, j=0;        
        while(j<4){
            do{
                a1=gerador.nextInt(10);
                a2=gerador.nextInt(4);
                if(!carta[a1][a2].isSaiu()){
                    cartas[i]=carta[a1][a2];
                    carta[a1][a2].setSaiu(true);
                    i++;
                }
            }while(i<3);
            jogadores[j].setCartas(cartas[0], cartas[1], cartas[2]);    
            j++;
            i=0;
        }
        for(i=0;i<4;i++) {
            System.out.println(jogadores[i]);
            //System.out.println(jogadores[i].maior());
            //System.out.println(jogadores[i].menor());
        }
    }

    public void jogadas(){
        
        if(j1>=3) this.acabouRod();
        if(k1==0){
            l1=this.comecavez();
            i1=l1;
        }
        // 4 cartas
        //jogador principal eh o 2
        //if(rodadas[nrodada].getRodada()[j1].getVencedor()!=null) return;
        if(i1==2) esperaeu=true;
        else{
            decisao.decide(k1, jogadores[i1],rodadas[nrodada].getRodada()[j1],cartadavez);
            ultima=rodadas[nrodada].getRodada()[j1];
            this.desenhaMesa();
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Truco.class.getName()).log(Level.SEVERE, null, ex);
            }
            k1++;
            i1++;
            if(i1>3) i1=0;
            if(k1>3){
                k1=0;
                j1++;
            }
        }
        if(j1>=3) this.acabouRod();
    }
    
    private void acabouRod(){
        j1=k1=0;
        rodadas[nrodada].setVencedor(dupla1, dupla2);
        ultima=rodadas[nrodada].getRodada()[j1];
        this.desenhaMesa();
        nrodada++;
        this.insereCartas();
        this.vira();
        this.daCarta();
    }

    private int comecavez() {
        if(index==3) index=-1;
        index++;
        return index;
    }
    
    private void minhaVez() {
        //aqui vai ser a vez do jogador verdadeiro, não da máquina
        this.escolheCarta();
        if(buffid.cima && !cartadavez.isJaJoagada()){
            buffid.cima=false;
            decisao.decide(k1,jogadores[i1],rodadas[nrodada].getRodada()[j1],cartadavez);            
            ultima=rodadas[nrodada].getRodada()[j1];
            this.desenhaMesa();
            esperaeu=false;
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Truco.class.getName()).log(Level.SEVERE, null, ex);
            }
            k1++;
            i1++;
            if(k1>3){
                k1=0;
                j1++;
            }
        }
    }

    public void carregaVerso() {
        try {
            versoCarta = ImageIO.read(new File(System.getProperty("user.dir") + "\\imagens\\100.png"));
            espaço = ImageIO.read(new File(System.getProperty("user.dir") + "\\imagens\\espaco.png"));
        } catch (IOException e) {
            System.out.println("Erro no carregamento da imagem do verso");

        }
    }

    @Override
    @SuppressWarnings("empty-statement")
    public void run() {
        //buffid.g2dbi.draw();
        
        //frames por segundo
        frames++;        
        long tempo=System.currentTimeMillis();                  
        if(tempo-time>=1000){            
            x= frames;
            time=System.currentTimeMillis();
            frames=0;
        }
        //System.out.println("FRAMES POR SEGUNDO: "+fps+"\n");
        
        Rectangle2D bg = new Rectangle2D.Double(0, 0, 1366, 768);

        buffid.g2dbi.setPaint(Color.GREEN);
        buffid.g2dbi.fill(bg);      
   
        buffid.g2dbi.setPaint(Color.WHITE);
        
        mostra = false;
        Font fonte;
        fonte = new Font("Verdana", Font.BOLD, 20);
        buffid.g2dbi.setFont(fonte);   
        
        this.desenhaBaralho();
        this.animacaoDistribuiCartas();
        this.animacaoVira();
        
        if(cont<fps*segAnimacao) cont++;        
        else{
            if(!esperaeu){
                this.jogadas();
                ultima=rodadas[nrodada].getRodada()[j1];
                this.desenhaMesa();
            }
            else{
                this.minhaVez();
                //ultima=rodadas[nrodada].getRodada()[j1];
                this.desenhaMesa();
            }
        }
        
        if(dupla1.getPontos()>=12){
            fonte = new Font("Verdana", Font.BOLD, 40);
            buffid.g2dbi.setFont(fonte);
            buffid.g2dbi.drawString("FIM, VENCEDOR"+dupla1.toString(), 200, 350);
            nrodada=0;
        }
        else if(dupla2.getPontos()>=12){
            fonte = new Font("Verdana", Font.BOLD, 40);
            buffid.g2dbi.setFont(fonte);
            buffid.g2dbi.drawString("FIM, VENCEDOR : "+dupla2.toString(), 200, 350);
            nrodada=0;
        }      
        else{           
            buffid.g2dbi.drawString(dupla1.toString()+": "+dupla1.getPontos()+" - "+dupla2.toString()+": "+dupla2.getPontos(),250,60);
            buffid.g2dbi.drawString(jogadores[0].getNome(), 370, 235);
            buffid.g2dbi.drawString(jogadores[1].getNome(), 30, 300);
            buffid.g2dbi.drawString(jogadores[3].getNome(), 1100, 300);
            buffid.g2dbi.drawString(jogadores[2].getNome(), 830, 500);
            buffid.g2dbi.drawString("FPS: "+x, 1250, 750);
            buffid.g2dbi.drawString("VIRA", 1100, 100);
        }
        buffid.repaint();
    }
    
    public void animacaoDistribuiCartas(){
        
            AffineTransform af_acc=new AffineTransform();        
            af_acc.scale(0.7, 0.7);
            //mostra=true;
            af_acc.translate(cont*passos[0][0],cont*passos[0][1]);
            for (int j = 0; j < 3; j++) {
                //Carta cartadavez;
                cartadavez = jogadores[0].getCarta(j);   

                af_acc.translate(cont*160/(fps*segAnimacao),00);
                if(!cartadavez.isJaJoagada())
                    if (mostra) {
                        buffid.g2dbi.drawImage(cartadavez.getImagem(), af_acc, buffid);

                    } else {
                       buffid.g2dbi.drawImage(versoCarta, af_acc, buffid);

                    }
                //salva posicao das cartas
                if(cont==fps*segAnimacao){
                    posicao[0]=(int) (50+ cont*passos[0][0]+j*(cont*160/(fps*segAnimacao)));
                    posicao[1]=(int) (50+cont*passos[0][1]);
                    jogadores[0].getCarta(j).setPosicao(posicao);
                }
            }
            af_acc=new AffineTransform();
            af_acc.translate(cont*passos[1][0],cont*passos[1][1]);
            af_acc.scale(0.7, 0.7);
            for (int j = 0; j < 3; j++) {
                //Carta cartadavez = null;
                cartadavez = jogadores[1].getCarta(j);
                af_acc.translate(cont*20/(fps*segAnimacao), cont*100/(segAnimacao*fps));
                if(!cartadavez.isJaJoagada())
                    if (mostra) {
                        buffid.g2dbi.drawImage(cartadavez.getImagem(), af_acc, buffid);
                    } else {
                        buffid.g2dbi.drawImage(versoCarta, af_acc, buffid);
                    }
                if(cont==fps*segAnimacao){
                    posicao[0]= (int) (50+ cont*passos[1][0]+j*(cont*20/(fps*segAnimacao)));
                    posicao[1]=(int) (50+cont*passos[1][1]+j*cont*100/(segAnimacao*fps));
                    jogadores[1].getCarta(j).setPosicao(posicao);
                }
            }
            af_acc=new AffineTransform();
            af_acc.translate(cont*passos[2][0],cont*passos[2][1]);
            af_acc.scale(0.7, 0.7);
            for (int j = 0; j < 3; j++) {
                //Carta cartadavez = null;
                cartadavez = jogadores[3].getCarta(j);
                //System.out.println("Segundos da animação:"+fps);
                af_acc.translate(cont*-20/(segAnimacao*fps),cont*100/(segAnimacao*fps));
                if(!cartadavez.isJaJoagada())
                    if (mostra) {
                        buffid.g2dbi.drawImage(cartadavez.getImagem(), af_acc, buffid);
                    } else {
                        buffid.g2dbi.drawImage(versoCarta, af_acc, buffid);

                }
                if(cont==fps*segAnimacao){
                    posicao[0]= (int) (50+ cont*passos[2][0]+j*(cont*-20/(segAnimacao*fps)));
                    posicao[1]=(int) (50+cont*passos[2][1]+j*(cont*100/(segAnimacao*fps)));
                    jogadores[3].getCarta(j).setPosicao(posicao);
                }
            }

            af_acc=new AffineTransform();
            af_acc.translate(cont*passos[3][0],cont*passos[3][1]);
            for (int j = 0; j < 3; j++) {
                //Carta cartadavez = null;
                cartadavez = jogadores[2].getCarta(j);
                mostra = true;
                af_acc.translate(cont*162/(segAnimacao*fps),0);
                if(!cartadavez.isJaJoagada())
                    if (mostra) {
                    buffid.g2dbi.drawImage(cartadavez.getImagem(), af_acc, buffid);
                    } else {
                   buffid.g2dbi.drawImage(versoCarta, af_acc, buffid);
                    }
                if(cont==fps*segAnimacao){
                    posicao[0]= (int)(50+ cont*passos[3][0]+j*(cont*160/(segAnimacao*fps)));
                    posicao[1]=(int) (50+cont*passos[3][1]);
                    jogadores[2].getCarta(j).setPosicao(posicao);
                }
            
            
        }
        
    }
    
    public void desenhaBaralho(){
        
        AffineTransform af_acc=new AffineTransform();
        af_acc.translate(50, 50);
        af_acc.scale(0.7, 0.7);
        for (int j = 0; j <27; j++) {
            af_acc.translate(.5,.5);
            buffid.g2dbi.drawImage(versoCarta, af_acc, buffid);
        }
    }
    
    public void desenhaMesa() {
        
            AffineTransform af_acc;
            Mesa mesa=ultima;
            if(mesa!=null){     
            
            Carta[] cartasMesa = mesa.getCartas();      
                for (Carta cartasMesa1 : cartasMesa) {
                    if (jogadores[2].pertence(cartasMesa1)) {
                        af_acc=new AffineTransform();
                        af_acc.translate(610,380);
                        af_acc.scale(0.5, 0.5);
                        buffid.g2dbi.drawImage(cartasMesa1.getImagem(), af_acc, buffid);
                    } else if (jogadores[0].pertence(cartasMesa1)) {
                        af_acc=new AffineTransform();                        
                        af_acc.translate(610,250);
                        af_acc.scale(0.5, 0.5);
                        buffid.g2dbi.drawImage(cartasMesa1.getImagem(), af_acc, buffid);
                    } else if (jogadores[1].pertence(cartasMesa1)) {
                        af_acc=new AffineTransform();
                        af_acc.translate(510,300);
                        af_acc.scale(0.5, 0.5);
                        buffid.g2dbi.drawImage(cartasMesa1.getImagem(), af_acc, buffid);
                    } else if (jogadores[3].pertence(cartasMesa1)) {
                        af_acc=new AffineTransform();
                        af_acc.translate(710,300);
                        af_acc.scale(0.5, 0.5);
                        buffid.g2dbi.drawImage(cartasMesa1.getImagem(), af_acc, buffid);
                    }
                }
            
            } else{
                af_acc=new AffineTransform();
                af_acc.translate(610,380);
                af_acc.scale(0.5, 0.5); 
                buffid.g2dbi.drawImage(this.espaço, af_acc, buffid);
                af_acc=new AffineTransform();
                af_acc.translate(610,250);
                af_acc.scale(0.5, 0.5); 
                buffid.g2dbi.drawImage(this.espaço, af_acc, buffid);
                af_acc=new AffineTransform();
                af_acc.translate(510,300);
                af_acc.scale(0.5, 0.5); 
                buffid.g2dbi.drawImage(this.espaço, af_acc, buffid);
                af_acc=new AffineTransform();
                af_acc.translate(710,300);
                af_acc.scale(0.5, 0.5);
                buffid.g2dbi.drawImage(this.espaço, af_acc, buffid);
            }
            
            buffid.g2dbi.drawString(dupla1.toString()+": "+dupla1.getPontos()+" - "+dupla2.toString()+": "+dupla2.getPontos(),250,60);
            buffid.g2dbi.drawString(jogadores[0].getNome(), 370, 235);
            buffid.g2dbi.drawString(jogadores[1].getNome(), 30, 300);
            buffid.g2dbi.drawString(jogadores[3].getNome(), 1100, 300);
            buffid.g2dbi.drawString(jogadores[2].getNome(), 830, 500);
            buffid.g2dbi.drawString("FPS: "+x, 1250, 750);
            buffid.g2dbi.drawString("VIRA", 1100, 100);
            
             buffid.repaint();     
    }
    
    public void animacaoVira(){
        AffineTransform af_acc=new AffineTransform();
        af_acc.translate(cont*1200/(segAnimacao*fps), cont*50/(segAnimacao*fps));
        af_acc.scale(0.7, 0.7);        
        buffid.g2dbi.drawImage(vira.getImagem(), af_acc, buffid);
    }
    
    public void escolheCarta(){
        AffineTransform af_acc=new AffineTransform();
        Carta cartaselecionada;
        boolean aux=false;       
        af_acc.scale(1.1, 1.1);
        af_acc.translate(363+153*buffid.getSelect(),475);
            cartaselecionada = jogadores[2].getCarta(buffid.getSelect());
            if(!cartaselecionada.isJaJoagada()){
                buffid.g2dbi.drawImage(cartaselecionada.getImagem(), af_acc, buffid); 
                cartadavez=cartaselecionada;            
            }     

    }

    public static void main(String[] argv) {

        int width = 1366;
        int height = 768;
        int fps=60;
        int delay = Math.round(1000/fps)+1;
        
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        BufferedImageDrawer bid = new BufferedImageDrawer(bi, width, height);

        Truco jogo = null;
       
        
        try {
            jogo = new Truco(bid, width, height, delay);
        } catch (IOException ex) {
            System.out.println("Erro de arquivo ao criar jogo.");
        }

        Timer t = new Timer();
        t.scheduleAtFixedRate(jogo, 0,delay);

        
    }
}
