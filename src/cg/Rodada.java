/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cg;

/**
 *
 * @author Gianfranco
 */
public class Rodada {
    
    private Mesa rodada[];
    private Dupla vencedor=null;
    private int num=0, valor=1;
 

    public Rodada(Jogador[] jogadores,int num) {
        rodada = new Mesa[3];
        for(int i=0;i<rodada.length;i++)
            rodada[i]= new Mesa(jogadores,i+1);
        this.num=num;
    }

    public Mesa[] getRodada() {
        return rodada;
    }
    
    public void Truco(){
        if (valor==1) valor=3;
        else valor=valor+3;
    }
        

    public void setVencedor(Dupla dupla1, Dupla dupla2) {
        int cont1=0,cont2=0;
        
        for (Mesa rodada1 : rodada) {
            if (rodada1.getVencedor() != null && dupla1.pertence(rodada1.getVencedor())) {
                cont1++;
            } else if (rodada1.getVencedor() != null && dupla2.pertence(rodada1.getVencedor())) {
                cont2++;
            }  
        }
        if(cont1>cont2) {
                dupla1.setPontos(valor);
                vencedor=dupla1;
                System.out.println("Vencedor da rodada "+num+": "+vencedor+"\n\n");
            }
        else if(cont2>cont1){
                dupla2.setPontos(valor);
                vencedor=dupla2;
                System.out.println("Vencedor da rodada "+num+": "+vencedor+"\n\n");
            }
    }
}
